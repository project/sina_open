<?php

/**
 * 获取授权令牌并在 Drupal 可登录
 */
function sina_open_page_t_login() {
  global $user, $base_url;
   
  if ($user->uid) {
    if ($tok = sina_open_get_access_token($user->uid)){
      drupal_set_message('该用户已经绑定微博账号');
      drupal_goto('user/' . $user->uid . '/sina_open');   
    }
  }
  
  module_load_include('php', 'sina_open', 'saetv2.ex.class');
  $key = variable_get('sina_open_oauth_consumer_key', '');
  $secret = variable_get('sina_open_oauth_consumer_secret', '');
  $o = new SaeTOAuthV2($key, $secret);
  $aurl = $o->getAuthorizeURL($base_url . '/sina_open/auth_callback');
  drupal_goto($aurl);
}

/**
 * The callback function for auth.
 */
function sina_open_auth_callback() {
  global $user, $base_url;
  
  if ($_REQUEST['code']) {
    module_load_include('php', 'sina_open', 'saetv2.ex.class');
    $key = variable_get('sina_open_oauth_consumer_key', '');
    $secret = variable_get('sina_open_oauth_consumer_secret', '');
    $o = new SaeTOAuthV2($key, $secret);
    
    $keys = array(
      'code' => $_REQUEST['code'],
      'redirect_uri' => $base_url . '/sina_open/auth_callback',
    );
    try {
      $token = $o->getAccessToken('code', $keys) ;
      $_SESSION['sina_open_token'] = $token;
    } catch (OAuthException $e) {   
    }

    if (isset($_SESSION['sina_open_token']['access_token'])) {
      // 查找是否已经绑定到其他账号
      $result = db_select('sina_open_user', 's')
        ->fields('s')
        ->condition('sina_uid', $_SESSION['sina_open_token']['uid'])
        ->execute()
        ->fetchObject();
      
      if ($user->uid > 0) {
        if (isset($result->uid) && $result->uid != $user->uid) {
          drupal_set_message('该帐号已与其它用户绑定', 'error');         
        }
        else {
          sina_open_user_bind();
          drupal_set_message('绑定成功');   
        }
        drupal_goto('user/' . $user->uid  . '/sina_open');
      }   
      else {
        if (isset($result->uid) && $result->uid > 0) {
          $form_state['uid'] = $result->uid;
        }
        else {
          $new_account = sina_open_user_signup();
          $form_state['uid'] = $new_account->uid;   
        }
        user_login_submit(array(), $form_state);
        drupal_goto('user/' . $form_state['uid']);
      }   
    }
  }
  
  drupal_goto();
}

/**
 * 微博相关设置
 * @param (object) $ac
 */
function sina_open_page_config($ac) { 
  $output = '';

  $data = db_query('SELECT uid, token, sina_name FROM {sina_open_user} WHERE uid = :uid', array(':uid' => $ac->uid))->fetchObject();
  
  if (isset($data->uid)) {
    $items[] = '您绑定的帐号：'. $data->sina_name
    . l('解除绑定', 'user/'.$ac->uid.'/sina_open/remove', array('attributes' => array('class' => 'sina_open_user_remove')));
    
    if ($GLOBALS['user']->uid == $ac->uid) {
      $items[] = drupal_render(drupal_get_form('sina_open_set_tweet_form', $ac->uid));
    }   
  } 
  else {
    $items[] = '您还没有绑定帐号，' . l('立即绑定', 'sina_open/t_login');
  }

  $output = theme('item_list', array('items' => $items, 'title' => '新浪微博', 'type' => 'ul', 'attributes' => array('id' => 'sina_open_page_config')));

  return $output;
}

/**
 * 解除新浪微博帐号绑定
 * @param (array) $form_state
 * @param (object) $ac
 */
function sina_open_remove_confirm($form, $form_state, $ac) {
  if ($data = db_query('SELECT token, sina_name FROM {sina_open_user} WHERE uid = :uid', array(':uid' => $ac->uid))->fetchObject()) {
    $form = array();
    $form['uid'] = array('#type' => 'hidden', '#value' => $ac->uid);
    $output = confirm_form($form,
                    '解除微博帐号绑定',
                    'user/' . $ac->uid . '/sina_open/config',
                    '当前绑定的帐号为：' . $data->sina_name . '，确定要解除绑定吗？此操作不可恢复，但不会影响已经发布到微博的数据。',
                    '解除绑定',
                    '取消');
    return $output;
  } 
  else {
    drupal_access_denied();
    exit;
  }
}

function sina_open_remove_confirm_submit($form, &$form_state) {
  db_delete('sina_open_user')
    ->condition('uid', $form_state['values']['uid'])
    ->execute();
  drupal_set_message('成功解除绑定');
  $form_state['redirect'] = 'user/'  .$form_state['values']['uid'] . '/sina_open/config';
  return;
}