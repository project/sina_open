<?php

/**
 * 管理设置
 */
function sina_open_admin_settings(){
  $form['sina_open_is_login'] = array(
    '#type' => 'radios',
    '#title' => '允许使用微博帐号登录',
    '#default_value' => variable_get('sina_open_is_login', 1),
    '#options' => array('不允许', '允许'),
    '#description' => '将自动创建一个新用户'
  );
  $form['sina_open_oauth_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => 'App Key',
    '#default_value' => variable_get('sina_open_oauth_consumer_key', NULL),
    '#description' => '你可以到 http://open.t.sina.com.cn/wiki/index.php 申请'
  );
  $form['sina_open_oauth_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => 'App Sercet',
    '#default_value' => variable_get('sina_open_oauth_consumer_secret', NULL),
  );
  $form['sina_open_comment'] = array(
    '#type' => 'radios',
    '#title' => '在评论中启用',
    '#default_value' => variable_get('sina_open_comment', 1),
    '#options' => array('否', '是')
  );
  
  $types = node_type_get_names();
  foreach ($types as $type => $name){
    $form['sina_open_node_'.$type] = array(
      '#title' => '在 ' . $name . ' 中启用',
      '#type' => 'checkbox',
      '#default_value' => variable_get('sina_open_node_' . $type, 1),
    );
  }
  
  $form['sina_open_button_url'] = array(
    '#title' => '图标地址',
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('sina_open_button_url', 'http://www.sinaimg.cn/blog/developer/wiki/240.png'),
  );
  
  return system_settings_form($form);
}

/**
 * 绑定了新浪微博帐号或通过新浪微博帐号注册的用户
 */
function sina_open_admin_users() {
  $query = db_select('sina_open_user', 's')->extend('PagerDefault');
  $query->innerJoin('users', 'u', 'u.uid = s.uid');
  $query->addField('u', 'name', 'name');
  $items = $query
    ->fields('s')
    ->limit(20)
    ->orderBy('u.uid', 'DESC')
    ->execute();
  
  foreach ($items as $item) {
    $table[] = array(
      theme('username', array('account' => $item)),
      '<a href="http://weibo.com/' . $item->sina_uid  . '">' . $item->sina_name . '</a>',
    );
  }
  
  if ($table) {
    $output =  theme('table', array('header' => array('用户', '微博账号'), 'rows' => $table));
    $output .= theme('pager');
  } else {
    $output = '没有数据';
  }
  
  return $output;
}